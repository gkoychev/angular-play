var express = require('express'),
    path = require('path'),
    logger = require('morgan');
var app = express();

process.chdir(__dirname);

app.use(logger('dev'));

app.use('/js', express.static(__dirname + '/js'));
app.use('/vendor', express.static(__dirname + '/vendor'));
app.use('/partials', express.static(__dirname + '/partials'));

app.get('/', function(req, res) {
    res.sendFile(path.resolve('./index.html'));
});
app.get('/configure', function(req, res) {
    res.sendFile(path.resolve('./index.html'));
});
app.get('/test', function(req, res) {
    res.sendFile(path.resolve('./index.html'));
});

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Listening at http://%s:%s', host, port);
});
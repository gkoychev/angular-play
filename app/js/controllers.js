(function(angular) {
    'use strict';

    angular.module('myControllers', [
        'myServices'
    ])

    .filter('highlight', function($sce) {
        return function(text, phrase) {
            if (phrase) text = text.replace(new RegExp('('+phrase+')', 'gi'),
                '<span class="bg-primary">$1</span>');
            return $sce.trustAsHtml(text)
        }
    })

    .controller('MenuCtrl', ['$scope', '$route', function($scope, $route){
            $scope.$route = $route;
            $scope.navCollapsed = true;
        }])

    .controller('ConfigCtrl', ['$scope', 'fields', function($scope, fields){
            $scope.fields = fields.data;
            $scope.selectAll = function(cat){
                fields.selectAllCategory(cat);
            };
            $scope.deselectAll = function(cat){
                fields.deselectAllCategory(cat);
            };
        }])

    .controller('TestCtrl', ['$scope', 'fields', function($scope, fields){
            $scope.fields = fields.data;
        }]);

})(window.angular);
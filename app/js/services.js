(function(angular) {
    'use strict';

    angular.module('myServices', [])
        .factory('fields', function(){

            var model = [
                {category_name: 'test 1', list: [
                    {name: 'Product Name', enabled: true,
                        model: 'product_name',
                        type: 'text',
                        placeholder: "Type here Product Name",
                        validation:{
                            required: true,
                            minlength: 2,
                            maxlength: 10
                        }},
                    {name: 'Qty', enabled: true,
                        model: 'product_qty',
                        type: 'number',
                        placeholder: "Type here Product Qty",
                        validation:{
                            required: true,
                            min: 0,
                            max: 1000
                        }}
                ]},
                {category_name: 'test 2', list: [
                    {name: 'Category Name', enabled: true,
                        model: 'category_name',
                        type: 'text',
                        placeholder: "Type here Category Name",
                        validation:{
                            required: false,
                            minlength: 2
                        }},
                    {name: 'Qty 1', enabled: false,
                        model: 'product_qty1',
                        type: 'number',
                        placeholder: "Type here Product Qty1",
                        validation:{
                            required: false,
                            min: 0,
                            max: 1000
                        }}
                ]},
                {category_name: 'test 3', list: [
                    {name: 'Product Url', enabled: true,
                        model: 'product_url',
                        type: 'url',
                        placeholder: "Type here Product Url",
                        validation:{
                            required: false
                        }},
                    {name: 'Email', enabled: true,
                        model: 'email',
                        type: 'email',
                        placeholder: "Type here Email",
                        validation:{
                            required: true
                        }}
                ]}
            ];

            return {
                data: model,
                selectAllCategory: function(category){
                    category.list.forEach(function(item){
                        item.enabled = true;
                    });
                },
                deselectAllCategory: function(category){
                    category.list.forEach(function(item){
                        item.enabled = false;
                    });
                }
            };
        })
    ;

})(window.angular);
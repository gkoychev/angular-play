(function(angular) {
    'use strict';

    //main app module
    var app = angular.module('myApp', [
        'ngRoute',
        'angular.filter',

        'jcs-autoValidate',

        'myControllers'

    ]).run([
        'bootstrap3ElementModifier',
        function (bootstrap3ElementModifier) {
            bootstrap3ElementModifier.enableValidationStateIcons(true);
        }
    ]);


    app.config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {

            //enable html5 history
            $locationProvider.html5Mode({
                enabled: true
            });

            //setup routes
            $routeProvider
                .when('/configure', {
                    templateUrl: 'partials/configure.html',
                    controller: 'ConfigCtrl',
                    controllerAs: 'config',
                    activetab: 'configure'
                })
                .when('/test', {
                    templateUrl: 'partials/test.html',
                    controller: 'TestCtrl',
                    controllerAs: 'test',
                    activetab: 'test'
                })
                .otherwise({
                    redirectTo: '/configure'
                });
        }
    ]);



})(window.angular);